"use strict";
// Ctrl + K + 1

// MIDI仕様
// Ch1: ベース + コード
// Ch2: メロディ

// v1.4.8 より Prettier 使用

const quarterNoteWidth = 40;
const noteHeight = 8;
const keyboardWidth = 50;
const maxNoteNotes = 2000;

const bottomY = 8 * 128;
let rightX = 1800;

let pianoRollRatioX = 1;

let capo = 0;
let noteMin = 0;

let capoDegree = 0;

const MIDI_CH_CHORD = 0;
const MIDI_CH_MELODY = 1;

let midiFileName = "Shell-Analyzer-output.mid";
let noteRectEndX = 0;

let bassNotes = [];

let header = {};

// 色の定義
const colorDefinition = {
	note: {
		normal: {
			// 発音していない静止状態のノートの色
			fillDefault: "#999",
			chord: "#09F", // 青
			shell: {
				chordTone: "#3C3", // 1, 5;	緑
				thirdTone: "#9C0", // 3;		黄緑
				seventhTone: "#FC0", // 7;		黄
				nonChordTone: "#F99", // 2, 4, 6;	赤
			},
			kernel: {
				tonic: "#3C3", // ド,	緑
				supertonic: "#9C0", // レ,	黄緑
				mediant: "#3C3", // ミ,	緑
				subdominant: "#F90", // ファ,橙
				dominant: "#3C3", // ソ,	緑
				submediant: "#9C0", // ラ,	黄緑
				leadingTone: "#FC0", // シ,	黄
				alteration: "#F99", // 黒鍵,赤
			},
		},
		sounding: {
			// 発音中のノートの色
			fillDefault: "#CCC",
			chord: "#6CF", // 青
			shell: {
				chordTone: "#6F6", // 1, 5;	緑
				thirdTone: "#9F6", // 3;		黄緑
				seventhTone: "#FF6", // 7;		黄
				nonChordTone: "#FCC", // 2, 4, 6;	赤
			},
			kernel: {
				tonic: "#6F6", // ド,	緑
				supertonic: "#9F6", // レ,	黄緑
				mediant: "#6F6", // ミ,	緑
				subdominant: "#FC9", // ファ,橙
				dominant: "#6F6", // ソ,	緑
				submediant: "#9F6", // ラ,	黄緑
				leadingTone: "#FF6", // シ,	黄
				alteration: "#FCC", // 黒鍵,赤
			},
		},
	},
	noteDegreeTextFillDefault: "#EEE",
	noteDegreeTextFillAlteredShell: "#09F",//"#F99", // 赤
	noteDegreeTextStrokeDefault: "#000",
	strokeDefault: "#000",
	background: {
		white: "#333",
		black: "rgb(35, 35, 35)", // Original: rgb(41, 41, 41)
	},
	keyboard: {
		white: "#FFF",
		black: "#333",
		stroke: "#000",
	},
	chordName: "#FFF",
	message: "#FFF",
	playCursor: "#FFF",
	barLine: {
		numberText: "rgba(255, 255, 255, 0.7)",
		head: "rgba(255, 255, 255, 0.3)",
		body: "rgba(255, 255, 255, 0.1)",
	},
};

let canvas = SVG("svg").size(rightX, bottomY);

let svgGroup = {};
svgGroup.background = canvas.group();
svgGroup.barLine = canvas.group();
svgGroup.main = canvas.group();
svgGroup.note = svgGroup.main.group();
svgGroup.chordAbsoluteName = svgGroup.main.group();
svgGroup.chordDegreeName = svgGroup.main.group();
svgGroup.chordNoteDegreeName = svgGroup.main.group();
svgGroup.playCursor = canvas.group();
svgGroup.keyboard = canvas.group();
svgGroup.message = canvas.group();
//group.main.draggable()

let svgContainer = {};
svgContainer.backgroundWhite = {};
svgContainer.backgroundBlacks = [];
svgContainer.keyboardWhites = [];
svgContainer.keyboardBlacks = [];

svgContainer.chordAbsoluteNameTexts = [];
svgContainer.chordDegreeNameTexts = [];
svgContainer.melodyNoteDegreeTexts = [];
svgContainer.melodyNoteDegreePatterns = [];
svgContainer.melodyNoteDegreePatternRects = [];
svgContainer.chordNameNumeratorAlphabetTspans = [];
svgContainer.chordNameNumeratorAccidentalTspans = [];
svgContainer.chordNameDenominatorAlphabetTspans = [];
svgContainer.chordNameDenominatorAccidentalTspans = [];
svgContainer.noteRects = [];
svgContainer.barLines = [];
svgContainer.barLineTexts = [];
svgContainer.message = {
	capo: {},
	warning: {},
	analyzingFormat: {},
};

let noteRectPropaties = [];

let isMovingMouse = false;
let isPressedShiftkey = false;
let isPressedCtrlkey = false;
let integratedMovementY = 0;
let originalScaleDegreeNames = "ド.レ♭.レ.ミ♭.ミ.ファ.ソ♭.ソ.ソ#.ラ.シ♭.シ".split(".");

let isPlayingAudition = false;
let isRunningAnimation = false;

const ANALYZING_FORMAT_SHELL_ONLY = 0;
const ANALYZING_FORMAT_KERNEL_ONLY = 1;
const ANALYZING_FORMAT_KERNEL_AND_SHELL = 2;
let analyzingFormat = ANALYZING_FORMAT_SHELL_ONLY;

let localStorageKey = {};
localStorageKey.soundVolume = "shell-analyzer.setting.sound.volume";

let prevSecond = 0;
let currentTempo = 120;
let audioContext = new AudioContext();
let gainNode = new GainNode(audioContext);
let oscillatorNodes = [];

function changeCursor() {
	const cursors = [
		[
			["default", "move"], // [0][0][any]
			["w-resize", "move"], // [0][1][any]
		],
		[
			["n-resize", "n-resize"], // [1][0][any]
			["move", "move"], // [1][1][any]
		],
	]; // [Shift][Ctrl][WheelClick]

	canvas.node.style.cursor = cursors[isPressedShiftkey ? 1 : 0][isPressedCtrlkey ? 1 : 0][isMovingMouse ? 1 : 0];
}
function windowResize() {
	rightX = document.body.clientWidth; //window.innerWidth;
	svgGroup.background.scale(rightX, 1);
	svgGroup.background.translate(0, 0); // 一応入れた
	canvas.width(rightX);
}
window.addEventListener("resize", windowResize);
document.addEventListener("keydown", (e) => {
	isPressedShiftkey = e.shiftKey;
	isPressedCtrlkey = e.ctrlKey;
	changeCursor();
	switch (e.key) {
		case " ":
			e.preventDefault();
			audition();
			break;
	}
});
document.addEventListener("keyup", (e) => {
	isPressedShiftkey = e.shiftKey;
	isPressedCtrlkey = e.ctrlKey;
	changeCursor();
});
canvas.node.addEventListener("wheel", (e) => {
	isPressedShiftkey = e.shiftKey;
	isPressedCtrlkey = e.ctrlKey;
	changeCursor();
	if (isPressedCtrlkey) {
		e.preventDefault();
		if (e.deltaY < 0) {
			// 上方向にホイールを回す
			zoomPianoRoll(2);
		} else if (e.deltaY > 0) {
			// 下方向にホイールを回す
			zoomPianoRoll(0.5);
		}
	}
});
canvas.node.addEventListener("mousedown", (e) => {
	//console.dir(e);
	isPressedShiftkey = e.shiftKey;
	isPressedCtrlkey = e.ctrlKey;
	switch (e.button) {
		case 0: // 左クリック
			if (e.target.nodeName === "tspan") {
				// クリックしたところが文字だった場合は何も起こさない
			} else {
				e.preventDefault();
				if (e.offsetX < keyboardWidth) {
				} else {
					if (!isPlayingAudition) {
						svgGroup.playCursor.x(e.offsetX);
					}
				}
			}
			break;

		case 1: // ホイールクリック
			e.preventDefault();
			isMovingMouse = true;
			integratedMovementY = 0;
			break;

		case 2: // 右クリック
			break;
	}
	changeCursor();
});
canvas.node.addEventListener("mousemove", (e) => {
	//console.log(
	//	e.x, e.y,
	//	e.clientX, e.clientY,
	//	e.layerX, e.layerY,
	//	e.movementX, e.movementY,
	//	e.offsetX, e.offsetY,
	//	e.pageX, e.pageY,
	//	e.screenX, e.screenY,
	//	integratedMovementY
	//);
	if (e.button !== 1) {
		// ホイールクリック 検出不可能
		//return;
	}
	if (!isMovingMouse) {
		return;
	}
	e.preventDefault();
	if (isPressedShiftkey) {
		const dy = Math.floor((integratedMovementY + e.movementY) / noteHeight) - Math.floor(integratedMovementY / noteHeight);
		integratedMovementY += e.movementY;
		if (dy !== 0) {
			transpose(-dy);
		}
	} else {
		moveNoteDx(e.movementX);
	}
	changeCursor();
});
canvas.node.addEventListener("mouseup", (e) => {
	isPressedShiftkey = e.shiftKey;
	isPressedCtrlkey = e.ctrlKey;
	if (e.button !== 1) {
		// ホイールクリック
		return;
	}
	e.preventDefault();
	isMovingMouse = false;
	integratedMovementY = 0;
	//console.dir(e)
	changeCursor();
});

function mainScreenOnMousedown(e) {
	console.dir(e);
}

//let backgroundOctaveLines = []; // 特に、途中で色を変更することもないので消した
function drawBackground() {
	const rightX = 1;
	const whiteColor = colorDefinition.background.white;
	const blackColor = colorDefinition.background.black;

	// 白鍵（というか一括で背景）を描画
	svgContainer.backgroundWhite = canvas.rect(rightX, bottomY).fill(whiteColor);
	svgGroup.background.add(svgContainer.backgroundWhite);

	for (let i = 0; i < 128 / 12; i++) {
		// シ to ド
		const y1 = bottomY - (i + 1) * 12 * noteHeight;
		svgGroup.background.add(canvas.line(0, y1, rightX, y1).stroke({width: 1, color: blackColor}));

		// シ to ド
		const y2 = bottomY - i * 12 * noteHeight - 5 * noteHeight;
		svgGroup.background.add(canvas.line(0, y2, rightX, y2).stroke({width: 1, color: blackColor}));
	}

	// 黒鍵部分
	const isBlacks = [0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0];
	let cnt = 0;
	for (let i = 0; i < 128; i++) {
		if (isBlacks[i % 12]) {
			svgContainer.backgroundBlacks[cnt] = canvas.rect(rightX, noteHeight);
			svgContainer.backgroundBlacks[cnt].fill(blackColor);
			svgContainer.backgroundBlacks[cnt].move(0, bottomY - i * noteHeight - noteHeight);
			svgGroup.background.add(svgContainer.backgroundBlacks[cnt]);
			cnt++;
		}
	}
}
function drawKeyboard() {
	const isBlacks = [0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0];
	const keyboardHeight = (noteHeight / 7) * 12;
	const keyboardWhiteWidth = keyboardWidth;
	const keyboardBlackWidth = (keyboardWhiteWidth * 4.8) / 7.9;

	// 白鍵を描画
	for (let i = 0; i < (128 / 12) * 7; i++) {
		svgContainer.keyboardWhites[i] = canvas.rect(keyboardWhiteWidth, keyboardHeight);
		svgContainer.keyboardWhites[i].fill(colorDefinition.keyboard.white);
		svgContainer.keyboardWhites[i].stroke(colorDefinition.keyboard.stroke);
		svgContainer.keyboardWhites[i].move(0, bottomY - i * keyboardHeight - keyboardHeight);
		svgGroup.keyboard.add(svgContainer.keyboardWhites[i]);
	}

	// 黒鍵を描画
	let cnt = 0;
	for (let i = 0; i < 128; i++) {
		if (isBlacks[i % 12]) {
			svgContainer.keyboardBlacks[cnt] = canvas.rect(keyboardBlackWidth, noteHeight);
			svgContainer.keyboardBlacks[cnt].fill(colorDefinition.keyboard.black);
			svgContainer.keyboardBlacks[cnt].stroke(colorDefinition.keyboard.stroke);
			svgContainer.keyboardBlacks[cnt].move(0, bottomY - i * noteHeight - noteHeight);
			svgGroup.keyboard.add(svgContainer.keyboardBlacks[cnt]);
			cnt++;
		}
	}
}
function drawPlayCursor() {
	const line = canvas.line(0, 0, 0, bottomY).stroke({width: 1, color: colorDefinition.playCursor});
	svgGroup.playCursor.add(line);
}

function demonstration() {
	afterLoadMidi(demonstrationMidiFileBinaryDataArrays, "夕焼け小焼け(デモMIDI)");
}

function initializeMainGroup() {
	svgGroup.barLine.x(keyboardWidth);
	svgGroup.main.x(keyboardWidth);
	svgGroup.main.y(0);
	svgGroup.playCursor.x(keyboardWidth);
	capo = 0;
	pianoRollRatioX = 1;
}
function finalizeMainGroup() {}

function getCapoString(c) {
	const capo2 = (c + 1200) % 12;
	const sign = ((c) => {
		if (c < 0) {
			return "-";
		} else if (c === 0) {
			return "";
		} else {
			return "+";
		}
	})(capo2);
	return sign + Math.abs(capo2);
}
function moveNoteDx(dx) {
	svgGroup.main.dx(dx * 1);
	svgGroup.barLine.dx(dx * 1);
	svgGroup.playCursor.dx(dx * 1);
}
function zoomPianoRoll(ratio) {
	// notes をいじってからなのか、描画系だけいじるのか
	// 今回は描画系だけいじります
	if (pianoRollRatioX * ratio < 2 ** -4) {
		return;
	} else if (pianoRollRatioX * ratio < 2 ** 4) {
		pianoRollRatioX *= ratio;
	} else {
		return;
	}
	const screenHalfWidth = rightX * 0.5;

	// ノートの拡大縮小
	for (let i = 0; i < svgContainer.noteRects.length; i++) {
		let noteRect = svgContainer.noteRects[i];
		noteRect.x(noteRect.x() * ratio);
		noteRect.width(noteRect.width() * ratio);
	}
	svgGroup.main.x((svgGroup.main.x() - screenHalfWidth) * ratio + screenHalfWidth);

	// 小節線横の小節数の拡大縮小
	for (let i = 0; i < svgContainer.barLineTexts.length; i++) {
		let barText = svgContainer.barLineTexts[i];
		barText.x(barText.x() * ratio);
	}

	// 小節線の位置の拡大縮小
	for (let i = 0; i < svgContainer.barLines.length; i++) {
		let barLine = svgContainer.barLines[i];
		barLine.x(barLine.x() * ratio);
	}
	svgGroup.barLine.x((svgGroup.barLine.x() - screenHalfWidth) * ratio + screenHalfWidth);

	svgGroup.playCursor.x((svgGroup.playCursor.x() - screenHalfWidth) * ratio + screenHalfWidth);
	// メロディノートの度数テキストの位置の拡大縮小
	for (let i = 0; i < svgContainer.melodyNoteDegreeTexts.length; i++) {
		let melodyNoteDegreeText = svgContainer.melodyNoteDegreeTexts[i];
		melodyNoteDegreeText.cx(melodyNoteDegreeText.cx() * ratio);
		//melodyNoteDegreeText.width(melodyNoteDegreeText.width() * ratio);
	}

	// メロディノートのパターンの拡大縮小
	for (let i = 0; i < svgContainer.melodyNoteDegreePatterns.length; i++) {
		let melodyNoteDegreePattern = svgContainer.melodyNoteDegreePatterns[i];
		melodyNoteDegreePattern.scale(pianoRollRatioX, 1);
	}

	// コード(絶対表記、度数表記)の位置の拡大縮小
	for (let i = 0; i < svgContainer.chordAbsoluteNameTexts.length; i++) {
		let chordAbsoluteNameText = svgContainer.chordAbsoluteNameTexts[i];
		let chordDegreeNameText = svgContainer.chordDegreeNameTexts[i];
		const cx = chordDegreeNameText.cx() * ratio;
		//chordAbsoluteNameText.x(chordAbsoluteNameText.x() * ratio);
		//chordAbsoluteNameText.cx(chordAbsoluteNameText.cx() * ratio);
		//chordAbsoluteNameText.cx(noteRects[bassNotes[i].count].cx() * ratio);
		//chordAbsoluteNameText.width(chordAbsoluteNameText.width() * ratio);
		// なぜか絶対表記だと失敗する　うんち
		chordAbsoluteNameText.cx(cx);
		chordDegreeNameText.cx(cx);
	}

	// コード()の位置の拡大縮小
	for (let i = 0; i < svgContainer.chordDegreeNameTexts.length; i++) {
		//chordDegreeNameText.width(chordDegreeNameText.width() * ratio);
	}
}
function redrawChordText(dn) {
	// 分子のコードのテキストを書き換え
	{
		const length = svgContainer.chordNameNumeratorAlphabetTspans.length;
		const chordRootNames = "C.C#.D.Eb.E.F.F#.G.G#.A.Bb.B"
			.replace(/#/g, "♯")
			.replace(/b/g, "♭")
			.split(".");
		for (let i = 0; i < length; i++) {
			const accidentalOrig = svgContainer.chordNameNumeratorAccidentalTspans[i].text();
			const rootOrig = svgContainer.chordNameNumeratorAlphabetTspans[i].text() + (accidentalOrig === "♯" || accidentalOrig === "♭" ? accidentalOrig : "");
			const rootNum = (chordRootNames.indexOf(rootOrig) + dn + 1200) % 12;
			const alphabet = chordRootNames[rootNum][0];
			const accidental = chordRootNames[rootNum][1] || " "; // このスペースは U+2005
			svgContainer.chordNameNumeratorAlphabetTspans[i].clear().text(alphabet);
			svgContainer.chordNameNumeratorAccidentalTspans[i].clear().text(accidental);
		}
	}

	// 分母のコードのテキストを書き換え
	{
		const length = svgContainer.chordNameDenominatorAlphabetTspans.length;
		const chordRootNames = "C.C#.D.Eb.E.F.F#.G.G#.A.Bb.B"
			.replace(/#/g, "♯")
			.replace(/b/g, "♭")
			.split(".");
		for (let i = 0; i < length; i++) {
			const accidentalOrig = svgContainer.chordNameDenominatorAccidentalTspans[i].text();
			const rootOrig = svgContainer.chordNameDenominatorAlphabetTspans[i].text()[1] + (accidentalOrig === "♯" || accidentalOrig === "♭" ? accidentalOrig : "");
			const rootNum = (chordRootNames.indexOf(rootOrig) + dn + 1200) % 12;
			const alphabet = chordRootNames[rootNum][0];
			const accidental = chordRootNames[rootNum][1] || " "; // このスペースは U+2005
			svgContainer.chordNameDenominatorAlphabetTspans[i].clear().text("/" + alphabet);
			svgContainer.chordNameDenominatorAccidentalTspans[i].clear().text(accidental);
		}
	}
}
function transpose(value) {
	let dn = value < 0 ? -1 : 1;
	for (let i = 0; i < 2; i++) {
		for (let k = 0; k < notes[i].length; k++) {
			const note = notes[i][k];
			note.note += dn;
			//noteRects[note.count].y(noteHeight * (127 - note.note));
		}
	}
	capo -= dn;
	const capoString = getCapoString(capo);
	svgContainer.message.capo.text("capo: " + capoString);
	//noteMin += dn;
	svgGroup.main.y(capo * noteHeight);
	//analyze(); // 重いので analyzeはしない

	redrawChordText(dn);
}
function pickRgbaToRgb(rgba){ // 現在未使用
	return rgba.replace(/rgba\((.+?),(.+?),(.+?),.+?\)/, "rgb($1,$2,$3)");
}
function pickRgbaToAlpha(rgba){ // 現在未使用
	return rgba.replace(/rgba\(.+?,.+?,.+?,(.+?)\)/, "$1") - 0;
}
function analyze() {
	// 初期化
	bassNotes = [];
	let chordNotes = [];

	// 最低音を検知（ついでにコードに使われてる音符をソート）
	let prevTime = 0;
	let prevGate = 0;
	let prevNote = 128;
	let prevNum = 0;
	let prevCount = 0;
	for (let k = 0; k < notes[MIDI_CH_CHORD].length; k++) {
		const note = notes[MIDI_CH_CHORD][k];
		if (prevTime < note.time) {
			chordNotes.sort((a, b) => (a < b ? -1 : 1)); // isSoundedNotes の判定がおかしくなるのでここでソート
			bassNotes.push({
				time: prevTime,
				gate: prevGate,
				note: prevNote,
				num: prevNum,
				count: prevCount,
				notes: chordNotes,
			});
			prevGate = note.gate;
			prevNote = note.note;
			prevNum = k;
			prevCount = note.count;
			chordNotes = [];
		}
		if (note.note < prevNote) {
			prevNote = note.note;
			prevCount = note.count;
		}
		chordNotes.push(note.note);

		prevTime = note.time;
	}
	bassNotes.push({
		time: prevTime,
		gate: prevGate,
		note: prevNote,
		num: prevNum,
		count: prevCount,
		notes: chordNotes,
	});

	// 最初の1個を削除
	if (bassNotes[0].note === 128) {
		bassNotes.shift();
	}

	// コードの判定 ＋ コードの名前の付加
	const inChordDegreeFillColorTable = [
		colorDefinition.note.normal.shell.chordTone,
		colorDefinition.note.normal.shell.thirdTone,
		colorDefinition.note.normal.shell.seventhTone,
		colorDefinition.note.normal.shell.nonChordTone,
	];
	const inChordDegreeFillColorSoundingNoteTable = [
		colorDefinition.note.sounding.shell.chordTone,
		colorDefinition.note.sounding.shell.thirdTone,
		colorDefinition.note.sounding.shell.seventhTone,
		colorDefinition.note.sounding.shell.nonChordTone,
	];
	let scaleDegreeNames = [];
	let scaleDegreeFillColors = []; // ホントは配列にしなくてもいいけど、途中での転調に対応する場合に備えて今から配列にしておく
	let inChordDegreeNames = [];
	let inChordDegreeFillColors = [];
	let chordNames = [];
	let chordDegreeNames = [];
	for (let i = 0; i < bassNotes.length; i++) {
		const bassNote = bassNotes[i];

		// コードの名前の付加
		const capoedNotes = bassNote.notes.map((value) => value + capo);
		const chordName = note2chord.names(bassNote.notes, true)[0]; // 第1引数は capoedNotes ではない！ v1.4.5
		const chordDegreeName = note2chord.degreeNames(capoedNotes, false, -capoDegree)[0];
		chordNames[i] = chordName;
		chordDegreeNames[i] = chordDegreeName;

		// メロディーノートの色と度数を変える
		const inChordDegreeNameBasics = [
			"1.b2.2.m3.3.4.b5.5.#5.6.7.7".split("."), // C Ionian アイオニアン
			"1.b2.2.m3.3.4.#4.5.#5.6.7.7".split("."),
			"1.b2.2.m3.3.4.b5.5.#5.6.7.7".split("."), // D Dorian
			"1.b2.2.m3.3.4.#4.5.#5.6.7.7".split("."),
			"1.b2.2.m3.3.4.b5.5.b6.6.7.7".split("."), // E Phrygian
			"1.b2.2.m3.3.4.#4.5.#5.6.7.7".split("."), // F Lydian
			"1.b2.2.m3.3.4.b5.5.b6.6.7.7".split("."),
			"1.b2.2.m3.3.4.#4.5.b6.6.7.7".split("."), // G Mixolydian
			"1.b2.2.m3.3.4.#4.5.#5.6.7.7".split("."),
			"1.b2.2.m3.3.4.#4.5.b6.6.7.7".split("."), // A Aeolian エオリアン
			"1.b2.2.m3.3.4.#4.5.#5.6.7.7".split("."),
			"1.b2.2.m3.3.4.b5.5.b6.6.7.7".split("."), // B Locrian
		];
		const inChordDegreeFillColorKeys = {
			"1"  : {normal: colorDefinition.note.normal.shell.chordTone,	sounding: colorDefinition.note.sounding.shell.chordTone},
			"b2" : {normal: colorDefinition.note.normal.shell.nonChordTone,	sounding: colorDefinition.note.sounding.shell.nonChordTone},
			"2"  : {normal: colorDefinition.note.normal.shell.nonChordTone,	sounding: colorDefinition.note.sounding.shell.nonChordTone},
			"#2" : {normal: colorDefinition.note.normal.shell.nonChordTone,	sounding: colorDefinition.note.sounding.shell.nonChordTone},
			"m3" : {normal: colorDefinition.note.normal.shell.thirdTone,	sounding: colorDefinition.note.sounding.shell.thirdTone},
			"3"  : {normal: colorDefinition.note.normal.shell.thirdTone,	sounding: colorDefinition.note.sounding.shell.thirdTone},
			"4"  : {normal: colorDefinition.note.normal.shell.nonChordTone,	sounding: colorDefinition.note.sounding.shell.nonChordTone},
			"#4" : {normal: colorDefinition.note.normal.shell.nonChordTone,	sounding: colorDefinition.note.sounding.shell.nonChordTone},
			"b5" : {normal: colorDefinition.note.normal.shell.chordTone,	sounding: colorDefinition.note.sounding.shell.chordTone},
			"5"  : {normal: colorDefinition.note.normal.shell.chordTone,	sounding: colorDefinition.note.sounding.shell.chordTone},
			"#5" : {normal: colorDefinition.note.normal.shell.chordTone,	sounding: colorDefinition.note.sounding.shell.chordTone},
			"b6" : {normal: colorDefinition.note.normal.shell.nonChordTone,	sounding: colorDefinition.note.sounding.shell.nonChordTone},
			"6"  : {normal: colorDefinition.note.normal.shell.nonChordTone,	sounding: colorDefinition.note.sounding.shell.nonChordTone},
			"m7" : {normal: colorDefinition.note.normal.shell.seventhTone,	sounding: colorDefinition.note.sounding.shell.seventhTone},
			"M7" : {normal: colorDefinition.note.normal.shell.seventhTone,	sounding: colorDefinition.note.sounding.shell.seventhTone},
			"7"  : {normal: colorDefinition.note.normal.shell.seventhTone,	sounding: colorDefinition.note.sounding.shell.seventhTone},
		};
		const scaleDegreeFillColorBasic = [
			colorDefinition.note.normal.kernel.tonic, // C
			colorDefinition.note.normal.kernel.alteration,
			colorDefinition.note.normal.kernel.supertonic, // D
			colorDefinition.note.normal.kernel.alteration,
			colorDefinition.note.normal.kernel.mediant, // E

			colorDefinition.note.normal.kernel.subdominant, // F
			colorDefinition.note.normal.kernel.alteration,
			colorDefinition.note.normal.kernel.dominant, // G
			colorDefinition.note.normal.kernel.alteration,
			colorDefinition.note.normal.kernel.submediant, // A
			colorDefinition.note.normal.kernel.alteration,
			colorDefinition.note.normal.kernel.leadingTone, // B
		];
		const scaleDegreeFillColorSoundingNoteBasic = [
			colorDefinition.note.sounding.kernel.tonic, // C
			colorDefinition.note.sounding.kernel.alteration,
			colorDefinition.note.sounding.kernel.supertonic, // D
			colorDefinition.note.sounding.kernel.alteration,
			colorDefinition.note.sounding.kernel.mediant, // E

			colorDefinition.note.sounding.kernel.subdominant, // F
			colorDefinition.note.sounding.kernel.alteration,
			colorDefinition.note.sounding.kernel.dominant, // G
			colorDefinition.note.sounding.kernel.alteration,
			colorDefinition.note.sounding.kernel.submediant, // A
			colorDefinition.note.sounding.kernel.alteration,
			colorDefinition.note.sounding.kernel.leadingTone, // B
		];
		const numeratorRootNum = (chordName.numerator.semitoneDistanceFromDenominator + bassNote.note + capo + capoDegree + 1200) % 12;
		const denominatorRootNum = (bassNote.note + capo + capoDegree + 1200) % 12;

		let currentInChordDegreeNumeratorNames = inChordDegreeNameBasics[numeratorRootNum];
		const currentInChordDegreeDenominatorNames = inChordDegreeNameBasics[denominatorRootNum];

		if (chordName.tension.match(/#9/)) {
			//  #9 のとき
			currentInChordDegreeNumeratorNames[3] = "#2";
		}

		if (chordName.tension.match(/b5/) || chordName.quality.match(/dim/)) {
			//  b5 のとき || dim のとき
			currentInChordDegreeNumeratorNames[6] = "b5";
		}
		if (chordName.tension.match(/#11/)) {
			// #11 のとき
			currentInChordDegreeNumeratorNames[6] = "#4";
		}

		if (chordName.quality.match(/aug/) || chordName.quality.match(/#5/)) {
			//  aug のとき || #5 のとき
			currentInChordDegreeNumeratorNames[8] = "#5";
		}
		if (chordName.tension.match(/b13/)) {
			// b13 のとき
			currentInChordDegreeNumeratorNames[8] = "b6";
		}

		inChordDegreeNames[i] = {
			numerator: currentInChordDegreeNumeratorNames,
			denominator: currentInChordDegreeDenominatorNames,
		};
		scaleDegreeNames[i] = originalScaleDegreeNames.map((_value, index, array) => {
			return array[(index + capoDegree + capo + 1200) % 12];
		});

		inChordDegreeFillColors[i] = {
			numerator: {},
			denominator: {},
		};
		inChordDegreeFillColors[i].numerator.normal = currentInChordDegreeNumeratorNames.map((value) => {
			return inChordDegreeFillColorKeys[value].normal;
		});
		inChordDegreeFillColors[i].numerator.sounding = currentInChordDegreeNumeratorNames.map((value) => {
			return inChordDegreeFillColorKeys[value].sounding;
		});
		inChordDegreeFillColors[i].denominator.normal = currentInChordDegreeDenominatorNames.map((value) => {
			return inChordDegreeFillColorKeys[value].normal;
		});
		inChordDegreeFillColors[i].denominator.sounding = currentInChordDegreeDenominatorNames.map((value) => {
			return inChordDegreeFillColorKeys[value].sounding;
		});

		scaleDegreeFillColors[i] = {};
		scaleDegreeFillColors[i].normal = scaleDegreeFillColorBasic.map((_value, index, array) => { // 順番を変えるだけ
			return array[(index + capoDegree + capo + 1200) % 12];
		});
		scaleDegreeFillColors[i].sounding = scaleDegreeFillColorSoundingNoteBasic.map((_value, index, array) => { // 順番を変えるだけ
			return array[(index + capoDegree + capo + 1200) % 12];
		});
	}

	// コードネームを描画する
	svgGroup.chordAbsoluteName.clear();
	svgGroup.chordDegreeName.clear();
	svgGroup.chordNoteDegreeName.clear();

	{
		const textY = noteHeight * (127 - noteMin + 2);
		let chordNameDenominatorCount = 0;
		for (let i = 0; i < bassNotes.length; i++) {
			const rect = svgContainer.noteRects[bassNotes[i].count];
			{
				// 具体的なコードネーム
				svgContainer.chordAbsoluteNameTexts[i] = canvas
					.text((add) => {
						const chordName = chordNames[i];
						const tension = chordName.tension;// + chordName.omission;
						{
							const root = chordName.numerator.alphabet;
							const accidental = chordName.numerator.accidental; // 臨時記号
							svgContainer.chordNameNumeratorAlphabetTspans[i] = add
								.tspan(root)
								.font({
									size: 25,
								})
								.attr("baseline-shift", "0");
							svgContainer.chordNameNumeratorAccidentalTspans[i] = add
								.tspan(accidental || " ")
								.font({
									// このスペースは U+2005
									size: 20,
								})
								.attr("baseline-shift", "super");
						}
						add.tspan(chordName.quality || " ")
							.font({
								// このスペースは U+2005
								size: 15,
							})
							.attr("baseline-shift", "0");
						if (tension) {
							add.tspan("(" + tension + ")")
								.font({
									size: 15,
								})
								.attr("baseline-shift", "super");
						}
						if (chordName.denominator.name) {
							const root = chordName.denominator.name[0];
							const accidental = chordName.denominator.name[1]; // 臨時記号

							svgContainer.chordNameDenominatorAlphabetTspans[chordNameDenominatorCount] = add
								.tspan("/" + root)
								.font({
									size: 20,
								})
								.attr("baseline-shift", "0");
							svgContainer.chordNameDenominatorAccidentalTspans[chordNameDenominatorCount] = add
								.tspan(accidental || " ")
								.font({
									// このスペースは U+2005
									size: 15,
								})
								.attr("baseline-shift", "super");
							chordNameDenominatorCount++;
						}
					})
					.y(textY)
					.cx(rect.cx())
					.font({
						fill: colorDefinition.chordName,
						family: "Helvetica",
					})
					.attr("cursor", "text");
					// .rebuild(false) が意味なかった。なぜじゃ～～～～
				svgGroup.chordAbsoluteName.add(svgContainer.chordAbsoluteNameTexts[i]);
			}
			{
				// 度数でのコードネーム
				let text = canvas
					.text((add) => {
						const chordName = chordDegreeNames[i];
						const tension = chordName.tension;// + chordName.omission;
						{
							const root = chordName.numerator.alphabet;
							const accidental = chordName.numerator.accidental; // 臨時記号
							add.tspan(accidental || " ")
								.font({
									// このスペースは U+2005
									size: 20,
								})
								.attr("baseline-shift", "super");
							add.tspan(root)
								.font({
									size: 25,
								})
								.attr("baseline-shift", "0");
						}
						add.tspan(chordName.quality || " ").font({
							// このスペースは U+2005
							size: 15,
						});
						if (tension !== "") {
							add.tspan("(" + tension + ")")
								.font({
									size: 15,
								})
								.attr("baseline-shift", "super");
						}
						if (chordName.denominator.name) {
							const rootStringLength = chordName.denominator.name.length;
							const root = chordName.denominator.name[rootStringLength - 1];
							const accidental = rootStringLength === 2 ? chordName.denominator.name[0] : ""; // 臨時記号
							add.tspan("/")
								.font({
									size: 20,
								})
								.attr("baseline-shift", "0");
							add.tspan(accidental || " ")
								.font({
									// このスペースは U+2005
									size: 15,
								})
								.attr("baseline-shift", "super");
							add.tspan(root)
								.font({
									size: 20,
								})
								.attr("baseline-shift", "0");
						}
					})
					.y(textY + 50)
					.cx(rect.cx())
					.font({
						fill: colorDefinition.chordName,
						family: "Helvetica",
					})
					.attr("cursor", "text");
				text.mouseover((e) => {
					//console.dir(e)
				});
				svgContainer.chordDegreeNameTexts[i] = text;
				svgGroup.chordDegreeName.add(text);
			}
		}
	}

	// メロディの度数判定
	{
		let cnt = 0;
		let cntText = 0;
		let cntPattern = 0;
		let cntPatternRect = 0;
		function changeColorAndDrawDegreeText(i, cnt, bassNoteTimes) {
			const inChordDegreeStrokeColor = colorDefinition.strokeDefault;
			const note = notes[MIDI_CH_MELODY][cnt];
			const noteRectPropatie = noteRectPropaties[note.count];
			let isFilledWidthPattern = false;
			noteRectPropatie.colorMaps = [];

			const scaleDegreeFillColor = scaleDegreeFillColors[i].normal[note.note % 12];
			const scaleDegreeFillColorSoundingNote = scaleDegreeFillColors[i].sounding[note.note % 12];

			if (bassNoteTimes) {
				// メロディ発音中にコードが変わるとき

				// melodyNoteDegreePatternRects にぼこぼこ pattern を push すればいい
				// 
				// svgContainer.melodyNoteDegreePatternRects[patternNum] にぼこぼこ box を push
				// noteRectPropatie.colorMaps にぼこぼこ push

				const rect = svgContainer.noteRects[note.count];
				const textY = rect.y() - 30;
				let pattern = canvas.pattern(rect.width(), noteHeight, () => {}).move(rect.x(), 0);
				let integratedWidth = 0;
				const scaleDegreeName = scaleDegreeNames[i][note.note % 12];
				let inChordDegreeFillColorObjects = [];

				svgContainer.melodyNoteDegreePatternRects[cntPattern] = [];

				pattern.update((add) => {
					for (let k = 0; k < bassNoteTimes.length; k++) {
						const ii = bassNoteTimes[k].i;
						const isSlashChord = chordNames[ii].numerator.semitoneDistanceFromDenominator !== 0;
						const numeratorRootNoteNote = chordNames[ii].numerator.semitoneDistanceFromDenominator + bassNotes[ii].note;
						const denominatorRootNoteNote = bassNotes[ii].note;
						const melodyNoteNote = notes[MIDI_CH_MELODY][cnt].note;
						const beforeStartingBassNote = !!bassNoteTimes[k].currentNoteSpreadAcrossFirstBassNoteTime;
						const inChordDegreeNumerator = (melodyNoteNote - numeratorRootNoteNote + 1200) % 12; // 1200 は負にならないようにするため
						const inChordDegreeDenominator = (melodyNoteNote - denominatorRootNoteNote + 1200) % 12; // 1200 は負にならないようにするため
						const inChordDegreeNumeratorName = beforeStartingBassNote ? "" : inChordDegreeNames[ii].numerator[inChordDegreeNumerator];
						const inChordDegreeDenominatorName = beforeStartingBassNote ? "" : inChordDegreeNames[ii].denominator[inChordDegreeDenominator];
						const inChordDegreeNumeratorFillColor = beforeStartingBassNote
							? colorDefinition.note.normal.fillDefault
							: inChordDegreeFillColors[ii].numerator.normal[inChordDegreeNumerator];
						const inChordDegreeNumeratorFillColorSoundingNote = beforeStartingBassNote
							? colorDefinition.note.sounding.fillDefault
							: inChordDegreeFillColors[ii].numerator.sounding[inChordDegreeNumerator];
						const inChordDegreeDenominatorFillColor = beforeStartingBassNote
							? colorDefinition.note.normal.fillDefault
							: inChordDegreeFillColors[ii].denominator.normal[inChordDegreeDenominator];
						const inChordDegreeDenominatorFillColorSoundingNote = beforeStartingBassNote
							? colorDefinition.note.sounding.fillDefault
							: inChordDegreeFillColors[ii].denominator.sounding[inChordDegreeDenominator];
						const textBoxWidth = k === bassNoteTimes.length - 1 ? rect.width() - integratedWidth : bassNoteTimes[k].width; // 最後の幅はベースの音符の幅のため、少し減らす
						let textFillColor = "";
						if (analyzingFormat === ANALYZING_FORMAT_SHELL_ONLY) {
							const isBlackKeyboards = [0, 1, 0, 1, 0,  0, 1, 0, 1, 0, 1, 0];
							const isAlteredShell = isBlackKeyboards[(melodyNoteNote + capoDegree + 1200) % 12];
							const noteDegreeTextFillColor = isAlteredShell ? colorDefinition.noteDegreeTextFillAlteredShell : colorDefinition.noteDegreeTextFillDefault;
							textFillColor = noteDegreeTextFillColor;
						} else if (analyzingFormat === ANALYZING_FORMAT_KERNEL_ONLY || true) {
							textFillColor = colorDefinition.noteDegreeTextFillDefault;
						} else {
							textFillColor = inChordDegreeNumeratorFillColor;
						}

						if(isSlashChord){
							{
								const addRect = add
									.rect(textBoxWidth, noteHeight * 0.5)
									.move(integratedWidth, 0)
									.fill(inChordDegreeNumeratorFillColor);
								svgContainer.melodyNoteDegreePatternRects[cntPattern].push(addRect);
		
								inChordDegreeFillColorObjects.push({
									normal: inChordDegreeNumeratorFillColor,
									sounding: inChordDegreeNumeratorFillColorSoundingNote,
								});
							}
							{
								const addRect = add
									.rect(textBoxWidth, noteHeight * 0.5)
									.move(integratedWidth, noteHeight * 0.5)
									.fill(inChordDegreeDenominatorFillColor);
								svgContainer.melodyNoteDegreePatternRects[cntPattern].push(addRect);
		
								inChordDegreeFillColorObjects.push({
									normal: inChordDegreeDenominatorFillColor,
									sounding: inChordDegreeDenominatorFillColorSoundingNote,
								});
							}
						}else{
							const addRect = add
								.rect(textBoxWidth, noteHeight)
								.move(integratedWidth, 0)
								.fill(inChordDegreeNumeratorFillColor);
							svgContainer.melodyNoteDegreePatternRects[cntPattern].push(addRect);
	
							inChordDegreeFillColorObjects.push({
								normal: inChordDegreeNumeratorFillColor,
								sounding: inChordDegreeNumeratorFillColorSoundingNote,
							});
						}

						noteRectPropatie.patternNum = cntPattern;

						if (analyzingFormat !== ANALYZING_FORMAT_KERNEL_ONLY) {
							// カーネルモード以外
							let textElement;
							if(isSlashChord){
								textElement = canvas.text((add) => {
									add.tspan(inChordDegreeNumeratorName)
										.dy(-10);
									add.tspan("/")
										.dy(10);
									add.tspan(inChordDegreeDenominatorName)
										.dy(0);
								});
							}else{
								textElement = canvas.text(inChordDegreeNumeratorName);
							}
							
							textElement.y(textY);
							textElement.cx(rect.x() + integratedWidth + textBoxWidth / 2);
							textElement.font({
								fill: textFillColor,
								stroke: chordDegreeNames.noteDegreeStrokeDefault,
								family: "游ゴシック Medium",
								size: 20,
							});
							textElement.attr("stroke-width", "2px");
							textElement.attr("paint-order", "stroke");
							svgContainer.melodyNoteDegreeTexts[cntText++] = textElement;
							svgGroup.chordNoteDegreeName.add(textElement);
						}
						integratedWidth += textBoxWidth;
					}
				});
				if (analyzingFormat === ANALYZING_FORMAT_SHELL_ONLY) {
					// シェルモード
					rect.fill(pattern);
					isFilledWidthPattern = true;
				} else if (analyzingFormat === ANALYZING_FORMAT_KERNEL_ONLY || true) {
					// カーネルモード
					const textFillColor = colorDefinition.noteDegreeTextFillDefault; // noteDegreeTextFillAlteredShell
					rect.fill(scaleDegreeFillColor);

					const text = canvas.text(scaleDegreeName);
					text.y(rect.y() - 30);
					text.cx(rect.cx());
					text.font({
						fill: textFillColor,
						stroke: colorDefinition.noteDegreeTextStrokeDefault,
						family: "游ゴシック Medium",
						size: 20,
					});
					text.attr("paint-order", "stroke");
					text.attr("stroke-width", "2px");
					svgContainer.melodyNoteDegreeTexts[cntText++] = text;
					svgGroup.chordNoteDegreeName.add(text);
					isFilledWidthPattern = false;
				} else {
					rect.fill(scaleDegreeFillColor);
					isFilledWidthPattern = false;
				}
				if (isFilledWidthPattern) {
					noteRectPropatie.colorMaps.push(...inChordDegreeFillColorObjects);
				} else {
					noteRectPropatie.colorMaps.push({
						normal: scaleDegreeFillColor,
						sounding: scaleDegreeFillColorSoundingNote,
					});
				}
				svgContainer.melodyNoteDegreePatterns[cntPattern++] = pattern;
				rect.stroke(inChordDegreeStrokeColor);
			} else {
				// メロディ発音中にコードが変わらないとき (多くの場合は単なる矩形塗りつぶし)
				const rect = svgContainer.noteRects[note.count];
				const numeratorRootNoteNote = chordNames[i].numerator.semitoneDistanceFromDenominator + bassNotes[i].note;
				const denominatorRootNoteNote = bassNotes[i].note;
				const melodyNoteNote = notes[MIDI_CH_MELODY][cnt].note;
				const inChordDegreeNumerator = (melodyNoteNote - numeratorRootNoteNote + 1200) % 12; // 1200 は負にならないようにするため
				const inChordDegreeDenominator = (melodyNoteNote - denominatorRootNoteNote + 1200) % 12; // 1200 は負にならないようにするため
				const beforeStartingBassNote = note.time + note.gate <= bassNotes[0].time;
				const inChordDegreeNumeratorName = beforeStartingBassNote ? "" : inChordDegreeNames[i].numerator[inChordDegreeNumerator];
				const inChordDegreeDenominatorName = beforeStartingBassNote ? "" : inChordDegreeNames[i].denominator[inChordDegreeDenominator];
				const inChordDegreeNumeratorFillColor = beforeStartingBassNote ? colorDefinition.note.normal.fillDefault : inChordDegreeFillColors[i].numerator.normal[inChordDegreeNumerator];
				const inChordDegreeNumeratorFillColorSoundingNote = beforeStartingBassNote ? colorDefinition.note.sounding.fillDefault : inChordDegreeFillColors[i].numerator.sounding[inChordDegreeNumerator];
				const scaleDegreeName = scaleDegreeNames[i][note.note % 12];
				let textFillColor = "";
				const isSlashChord = chordNames[i].numerator.semitoneDistanceFromDenominator !== 0;
				let textElement;
				if(isSlashChord && analyzingFormat === ANALYZING_FORMAT_SHELL_ONLY){
					let plainText = "";
					const isBlackKeyboards = [0, 1, 0, 1, 0,  0, 1, 0, 1, 0, 1, 0];
					const isAlteredShell = isBlackKeyboards[(melodyNoteNote + capoDegree + 1200) % 12];
					const noteDegreeTextFillColor = isAlteredShell ? colorDefinition.noteDegreeTextFillAlteredShell : colorDefinition.noteDegreeTextFillDefault;



					let pattern = canvas.pattern(rect.width(), noteHeight, () => {}).move(rect.x(), 0);
					let inChordDegreeFillColorObjects = [];
	
					svgContainer.melodyNoteDegreePatternRects[cntPattern] = [];
	
					pattern.update((add) => {
						const inChordDegreeDenominatorFillColor = beforeStartingBassNote
							? colorDefinition.note.normal.fillDefault
							: inChordDegreeFillColors[i].denominator.normal[inChordDegreeDenominator];
						const inChordDegreeDenominatorFillColorSoundingNote = beforeStartingBassNote
							? colorDefinition.note.sounding.fillDefault
							: inChordDegreeFillColors[i].denominator.sounding[inChordDegreeDenominator];
						const textBoxWidth = rect.width();

						{
							const addRect = add
								.rect(textBoxWidth, noteHeight * 0.5)
								.move(0, 0)
								.fill(inChordDegreeNumeratorFillColor);
							svgContainer.melodyNoteDegreePatternRects[cntPattern].push(addRect);
	
							inChordDegreeFillColorObjects.push({
								normal: inChordDegreeNumeratorFillColor,
								sounding: inChordDegreeNumeratorFillColorSoundingNote,
							});
						}
						{
							const addRect = add
								.rect(textBoxWidth, noteHeight * 0.5)
								.move(0, noteHeight * 0.5)
								.fill(inChordDegreeDenominatorFillColor);
							svgContainer.melodyNoteDegreePatternRects[cntPattern].push(addRect);
	
							inChordDegreeFillColorObjects.push({
								normal: inChordDegreeDenominatorFillColor,
								sounding: inChordDegreeDenominatorFillColorSoundingNote,
							});
						}

						noteRectPropatie.patternNum = cntPattern;
					});
					rect.fill(pattern);
					rect.stroke(inChordDegreeStrokeColor);
					noteRectPropatie.colorMaps.push(...inChordDegreeFillColorObjects);
					svgContainer.melodyNoteDegreePatterns[cntPattern++] = pattern;

					textFillColor = noteDegreeTextFillColor;
					plainText = inChordDegreeNumeratorName + "/" + inChordDegreeDenominatorName;
					if(analyzingFormat === ANALYZING_FORMAT_SHELL_ONLY){
						textElement = canvas.text((add) => {
							add.tspan(inChordDegreeNumeratorName)
								.dy(-10);
							add.tspan("/")
								.dy(10);
							add.tspan(inChordDegreeDenominatorName)
								.dy(0);
						});
					}else{
						textElement = canvas.text(plainText);
					}

					isFilledWidthPattern = true;
				}else{
					let plainText = "";
					let rectFillColor = "";
					let rectFillColorSoundingNote = "";
					if (analyzingFormat === ANALYZING_FORMAT_SHELL_ONLY) {
						const isBlackKeyboards = [0, 1, 0, 1, 0,  0, 1, 0, 1, 0, 1, 0];
						const isAlteredShell = isBlackKeyboards[(melodyNoteNote + capoDegree + 1200) % 12];
						const noteDegreeTextFillColor = isAlteredShell ? colorDefinition.noteDegreeTextFillAlteredShell : colorDefinition.noteDegreeTextFillDefault;
						rectFillColor = inChordDegreeNumeratorFillColor;
						rectFillColorSoundingNote = inChordDegreeNumeratorFillColorSoundingNote;
						textFillColor = noteDegreeTextFillColor;
						plainText = inChordDegreeNumeratorName;
					} else if (analyzingFormat === ANALYZING_FORMAT_KERNEL_ONLY || true) {
						rectFillColor = scaleDegreeFillColor;
						rectFillColorSoundingNote = scaleDegreeFillColorSoundingNote;
						textFillColor = colorDefinition.noteDegreeTextFillDefault;
						plainText = scaleDegreeName;
					} else {
						rectFillColor = scaleDegreeFillColor;
						rectFillColorSoundingNote = scaleDegreeFillColorSoundingNote;
						textFillColor = inChordDegreeNumeratorFillColor;
						plainText = inChordDegreeNumeratorName;
					}
					rect.fill(rectFillColor);
	
					rect.stroke(inChordDegreeStrokeColor);
					noteRectPropatie.colorMaps.push({
						normal: rectFillColor,
						sounding: rectFillColorSoundingNote,
					});
	
					textElement = canvas.text(plainText);
					
					isFilledWidthPattern = false;
				}
				textElement.y(rect.y() - 30);
				textElement.cx(rect.cx());
				textElement.font({
					fill: textFillColor,
					stroke: colorDefinition.noteDegreeTextStrokeDefault,
					family: "游ゴシック Medium",
					size: 20,
				});
				textElement.attr("paint-order", "stroke");
				textElement.attr("stroke-width", "2px");
				svgContainer.melodyNoteDegreeTexts[cntText++] = textElement;
				svgGroup.chordNoteDegreeName.add(textElement);
			}
			noteRectPropatie.isFilledWidthPattern = isFilledWidthPattern;
		}
		for (let i = 0; i < bassNotes.length; i++) {
			let nextBassNoteTime = 0;
			const melodyNotesLength = notes[MIDI_CH_MELODY].length;
			while (
				cnt < melodyNotesLength &&
				notes[MIDI_CH_MELODY][cnt].time <
					(nextBassNoteTime =
						bassNotes[i + 1] === undefined
							? Math.max(bassNotes[i].time + bassNotes[i].gate, notes[MIDI_CH_MELODY][cnt].time + notes[MIDI_CH_MELODY][cnt].gate)
							: bassNotes[i + 1].time)
			) {
				// bassNotes[i + 1].time を bassNotes[i].time + bassNotes[i].gate にすればよいかと思ったけど、これだと音符が短いときに対応できないね
				// 和音に対応できない件、i を一旦記憶しておいてそこまで戻すと良さそう
				const melodyNote = notes[MIDI_CH_MELODY][cnt];
				let melodyChannelHasChord = false;
				const prevI = i;
				if (cnt < melodyNotesLength - 1) {
					const nextMelodyNote = notes[MIDI_CH_MELODY][cnt + 1];
					if (nextMelodyNote.time < melodyNote.time + melodyNote.gate) {
						melodyChannelHasChord = true;
					}
				}
				const firstBassNoteTime = bassNotes[0].time;
				const currentNoteSpreadAcrossFirstBassNoteTime = melodyNote.time < firstBassNoteTime && firstBassNoteTime < melodyNote.time + melodyNote.gate; // 今のメロディノートが最初のベースノートの開始点をまたいでいるとき
				if (nextBassNoteTime < melodyNote.time + melodyNote.gate || currentNoteSpreadAcrossFirstBassNoteTime) {
					// メロディ発声中にコードが変わっているとき
					let bassNoteTimes = [];
					let dt = nextBassNoteTime - melodyNote.time;
					if (currentNoteSpreadAcrossFirstBassNoteTime) {
						const dw = firstBassNoteTime - melodyNote.time;
						bassNoteTimes.push({
							width: gateTimeToPixel(dw),
							i: 0,
							currentNoteSpreadAcrossFirstBassNoteTime: true,
						});
						dt -= dw;
					}
					if (i < bassNotes.length && dt) {
						bassNoteTimes.push({
							width: gateTimeToPixel(dt),
							i: i,
						});
						i++;
					}
					while (i < bassNotes.length && melodyNote.time + melodyNote.gate > bassNotes[i].time) {
						bassNoteTimes.push({
							width: gateTimeToPixel(bassNotes[i].gate),
							i: i,
						});
						i++;
					}
					i--;
					changeColorAndDrawDegreeText(i, cnt, bassNoteTimes);
					cnt++;
				} else {
					let bassNoteTimes = [];
					{
						//bassNoteTimes.push({
						//	width: gateTimeToPixel(melodyNote.gate),
						//	i: i,
						//});
					}
					changeColorAndDrawDegreeText(i, cnt);
					cnt++;
				}
				if (melodyChannelHasChord) {
					i = Math.max(prevI - 1, 0); // -1 を外すとうまくいかなくなるので NG
				}
			}
		}
	}
}
function changeCapoDegree(x) {
	capoDegree = x;
	analyze();
	redrawChordText(0);
}
function changeAnalyzingFormat(x) {
	switch (x) {
		case 0:
			analyzingFormat = ANALYZING_FORMAT_SHELL_ONLY;
			break;
		case 1:
			analyzingFormat = ANALYZING_FORMAT_KERNEL_ONLY;
			break;
		default:
		case 2:
			analyzingFormat = ANALYZING_FORMAT_KERNEL_AND_SHELL;
			break;
	}
	svgContainer.message.analyzingFormat.text(drawAnalyzingFormatText(x));
	analyze();
	redrawChordText(0);
}
function changeKernelChar(x) {
	const table = [
		"ド.レ♭.レ.ミ♭.ミ.ファ.ソ♭.ソ.ソ#.ラ.シ♭.シ".split("."),
		"1.b2.2.b3.3.4.b5.5.#5.6.7.7".split("."),
		"ⅰ.bⅱ.ⅱ.bⅲ.ⅲ.ⅳ.bⅴ.ⅴ.#ⅴ.ⅵ.ⅶ.ⅶ".split("."),
		"c.d♭.d.e♭.e.f.g♭.g.g#.a.b♭.b".split("."),
		"C.D♭.D.E♭.E.F.G♭.G.G#.A.B♭.B".split("."),
	];
	if (x < table.length) {
		originalScaleDegreeNames = table[x];
	} else {
		originalScaleDegreeNames = table[0];
	}
	analyze();
	redrawChordText(0);
}
function setAuditionSoundVolume(value) {
	if (value < 0 || 100 < value) {
		return;
	}
	gainNode.gain.value = value / 100; // value: 0-100
	document.getElementById("sound-volume").value = value;
	document.getElementById("sound-volume-value").innerHTML = (value * 1).toFixed(0);
	localStorage.setItem(localStorageKey.soundVolume, (value * 1).toFixed(0));
}
function switchChordNameDisplay(checked) {
	if (checked) {
	}
}
function exportPng() {
	const widthOrig = canvas.width();
	const paddingRight = 50;
	const maxWidth = 10000;
	const width = Math.min(maxWidth, noteRectEndX * pianoRollRatioX + svgGroup.main.x() + paddingRight);
	svgGroup.background.scale(width, 1);
	svgGroup.background.translate(0, 0); // これがないと何故か背景が左にずれる
	canvas.width(width);
	svg2png(canvas.node, midiFileName);
	canvas.width(widthOrig);
	svgGroup.background.scale(widthOrig, 1);
	svgGroup.background.translate(0, 0); // これがないと何故か背景が右にずれる
}
function getCurrentCursorPosition() {
	return (svgGroup.playCursor.x() - svgGroup.main.x()) / pianoRollRatioX;
}
function restoreNoteRectFillColor() {}
function setNoteRectFillColor() {}
let prveCursorX = 0;
let prevCountI = 0;
let requestAnimationFrameId = 0;
function setAllNoteToNormal() {
	for (let i = 0; i < svgContainer.noteRects.length; i++) {
		const noteRectPropatie = noteRectPropaties[i];
		if (noteRectPropatie.isFilledWidthPattern) {
			// パターン用
			const patternNum = noteRectPropatie.patternNum;
			const melodyNoteDegreePatternRects = svgContainer.melodyNoteDegreePatternRects[patternNum];
			for (let k = 0; k < melodyNoteDegreePatternRects.length; k++) {
				const melodyNoteDegreePatternRect = melodyNoteDegreePatternRects[k];
				melodyNoteDegreePatternRect.fill(noteRectPropatie.colorMaps[k].normal);
			}
		} else {
			// ただの四角形用
			const noteRect = svgContainer.noteRects[i];
			noteRect.fill(noteRectPropatie.colorMaps[0].normal);
		}
	}
}
function startAnimation() {
	if (isRunningAnimation) {
		return;
	}

	prveCursorX = getCurrentCursorPosition() - 1e-6;

	// 終端以降にカーソルがある場合は最初に戻して最初から再生する
	if (noteRectEndX < prveCursorX) {
		svgGroup.barLine.x(keyboardWidth);
		svgGroup.main.x(keyboardWidth);
		svgGroup.playCursor.x(keyboardWidth);
		prveCursorX = getCurrentCursorPosition() - 1e-6;
	}

	if (svgGroup.playCursor.x() < keyboardWidth) {
		// カーソルが画面左側外であれば、視点をカーソルの位置まで移動させる
		const dx = keyboardWidth - svgGroup.playCursor.x();
		moveNoteDx(dx);
	}

	currentTempo = get("#current-tempo").value * 1;
	isRunningAnimation = true;
	prevSecond = performance.now() / 1000;

	const channelNumbers = [MIDI_CH_CHORD, MIDI_CH_MELODY];
	const beatPerSecond = currentTempo / 60;
	const startGateTime = pixelToGateTime(getCurrentCursorPosition());
	for (let i = 0; i < channelNumbers.length; i++) {
		const channelNumber = channelNumbers[i];
		for (let k = 0; k < notes[channelNumber].length; k++) {
			const note = notes[channelNumber][k];
			const t0 = audioContext.currentTime + (note.time - startGateTime) / header.resolution / beatPerSecond;
			const t1 = audioContext.currentTime + (note.time + note.gate - startGateTime) / header.resolution / beatPerSecond;
			if (note.time + note.gate < startGateTime) {
				continue;
			}
			if (t0 < 0) {
				continue;
			}
			let oscillatorNode = new OscillatorNode(audioContext);
			oscillatorNode.frequency.value = 440 * 2 ** ((note.note - 69) / 12);
			oscillatorNode.type = "triangle";
			oscillatorNode.connect(gainNode).connect(audioContext.destination);
			oscillatorNode.start(t0);
			oscillatorNode.stop(t1);
			oscillatorNodes.push(oscillatorNode);
		}
	}

	// 最初のノートの色を変える
	{
		const currentCursorX = getCurrentCursorPosition();
		for (let i = prevCountI; i < svgContainer.noteRects.length; i++) {
			const noteRectPropatie = noteRectPropaties[i];
			if (noteRectPropatie.isFilledWidthPattern) {
				// パターン用
				const patternNum = noteRectPropatie.patternNum;
				const melodyNoteDegreePatternRects = svgContainer.melodyNoteDegreePatternRects[patternNum];
				const patternX = svgContainer.melodyNoteDegreePatterns[patternNum].x();
				for (let k = 0; k < melodyNoteDegreePatternRects.length; k++) {
					const melodyNoteDegreePatternRect = melodyNoteDegreePatternRects[k];
					const noteRectX = patternX + melodyNoteDegreePatternRect.x(); // pianoRollRatioX で除す必要なし
					const noteRectEndX = noteRectX + melodyNoteDegreePatternRect.width(); // pianoRollRatioX で除す必要なし
					if (noteRectX <= currentCursorX && currentCursorX < noteRectEndX) {
						melodyNoteDegreePatternRect.fill(noteRectPropatie.colorMaps[k].sounding);
					}
				}
			} else {
				// ただの四角形用
				const noteRect = svgContainer.noteRects[i];
				const noteRectX = noteRect.x() / pianoRollRatioX;
				const noteRectEndX = noteRectX + noteRect.width() / pianoRollRatioX;
				if (noteRectX <= currentCursorX && currentCursorX < noteRectEndX) {
					noteRect.fill(noteRectPropatie.colorMaps[0].sounding);
				}
			}
		}
	}

	animation();
}
function stopAnimation() {
	if (!isRunningAnimation) {
		return;
	}
	setAllNoteToNormal();
	isRunningAnimation = false;
	for (let i = 0; i < oscillatorNodes.length; i++) {
		oscillatorNodes[i].stop();
	}
	oscillatorNodes = [];
}
function animation() {
	if (!isRunningAnimation) {
		return;
	}
	const currentCursorX = getCurrentCursorPosition();

	// ノートの終端に来たら試聴を停止する
	if (noteRectEndX < currentCursorX) {
		audition();
		return;
	}

	if (svgGroup.playCursor.x() < rightX * 0.9) {
	} else {
		// 画面の90%に行ったらカーソルが10%の位置に来るように視点を移動させる
		const dx = rightX * 0.1 - svgGroup.playCursor.x();
		moveNoteDx(dx);
	}

	const prefetchTime = 100e-3; // 100ms
	const nowSecond = performance.now() / 1000;
	const dt = nowSecond - prevSecond;
	const beatPerSecond = currentTempo / 60;
	prevSecond = nowSecond;

	// ノートの色を変える
	for (let i = prevCountI; i < svgContainer.noteRects.length; i++) {
		const noteRectPropatie = noteRectPropaties[i];
		if (noteRectPropatie.isFilledWidthPattern) {
			// パターン用
			const patternNum = noteRectPropatie.patternNum;
			const melodyNoteDegreePatternRects = svgContainer.melodyNoteDegreePatternRects[patternNum];
			const patternX = svgContainer.melodyNoteDegreePatterns[patternNum].x();
			for (let k = 0; k < melodyNoteDegreePatternRects.length; k++) {
				const melodyNoteDegreePatternRect = melodyNoteDegreePatternRects[k];
				const noteRectX = patternX + melodyNoteDegreePatternRect.x(); // pianoRollRatioX で除す必要なし
				const noteRectEndX = noteRectX + melodyNoteDegreePatternRect.width(); // pianoRollRatioX で除す必要なし
				if (prveCursorX <= noteRectX && noteRectX < currentCursorX) {
					melodyNoteDegreePatternRect.fill(noteRectPropatie.colorMaps[k].sounding);
				}
				if (prveCursorX <= noteRectEndX && noteRectEndX < currentCursorX) {
					melodyNoteDegreePatternRect.fill(noteRectPropatie.colorMaps[k].normal);
				}
				if (noteRectEndX < prveCursorX) {
					//break;
				}
			}
		} else {
			// ただの四角形用
			const noteRect = svgContainer.noteRects[i];
			const noteRectX = noteRect.x() / pianoRollRatioX;
			const noteRectEndX = noteRectX + noteRect.width() / pianoRollRatioX;
			if (prveCursorX <= noteRectX && noteRectX < currentCursorX) {
				noteRect.fill(noteRectPropatie.colorMaps[0].sounding);
			}
			if (prveCursorX <= noteRectEndX && noteRectEndX < currentCursorX) {
				noteRect.fill(noteRectPropatie.colorMaps[0].normal);
			}
			if (noteRectEndX < prveCursorX) {
				//break;
			}
		}
	}
	prveCursorX = currentCursorX;

	svgGroup.playCursor.dx(dt * quarterNoteWidth * beatPerSecond * pianoRollRatioX);
	requestAnimationFrameId = requestAnimationFrame(animation);
}
function movePlayCursor(deltaBar) {
	if (isPlayingAudition) {
		if (requestAnimationFrameId) {
			cancelAnimationFrame(requestAnimationFrameId);
		}
		stopAnimation();
		svgGroup.playCursor.dx(pianoRollRatioX * quarterNoteWidth * 4 * deltaBar);
		startAnimation();
	} else {
		svgGroup.playCursor.dx(pianoRollRatioX * quarterNoteWidth * 4 * deltaBar);
	}
}
function rewindAudition() {
	// 早送り
	movePlayCursor(-2); // 2小節移動
}
function fastForwardAudition() {
	// 早送り
	movePlayCursor(2); // 2小節移動
}
function audition() {
	// 試聴
	isPlayingAudition = !isPlayingAudition;
	let iconElement = get("#audition-play-pause");
	iconElement.src = isPlayingAudition ? "img/ui-icon/audition-pause.png" : "img/ui-icon/audition-play.png";
	iconElement.alt = isPlayingAudition ? "一時停止 (Space)" : "再生 (Space)";
	iconElement.title = isPlayingAudition ? "一時停止 (Space)" : "再生 (Space)";
	if (isPlayingAudition) {
		startAnimation();
	} else {
		stopAnimation();
	}
	prevCountI = 0;
}
function analyzeKey() {
	let datas = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	for (let k = 0; k < notes[MIDI_CH_MELODY].length; k++) {
		const note = notes[MIDI_CH_MELODY][k];
		datas[note.note % 12] += note.gate;
	}
	const keyFrequencyConst = [1, 0, 1, 0, 1, 0.5, 0, 1, 0, 1, 0, 0.75]; // 筆者の主観でのメロディの頻度
	let frequencyAtKey = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
	for (let key = 0; key < 12; key++) {
		for (let i = 0; i < 12; i++) {
			frequencyAtKey[key] += keyFrequencyConst[(key + i) % 12] * datas[i % 12];
		}
	}
	{
		let text = "";
		const chordRootNames = "C.C#.D.Eb.E.F.F#.G.G#.A.Bb.B".replace(/#/g, "♯").split(".");
		for (let i = 0; i < 12; i++) {
			text += chordRootNames[i] + ":\t" + frequencyAtKey[(12 - i) % 12] + "\n";
		}
		console.log(text);
	}
	capoDegree = frequencyAtKey.indexOf(Math.max.apply(null, frequencyAtKey)); // 最大値のインデックスを返す (最大値が複数あった場合はインデックスの最小値が選ばれる)
	const index = capoDegree;
	get("#melody-key").selectedIndex = index;
	for (let i = 0; i < 12; i++) {
		const option = get("#melody-key").options[i];
		option.text = option.text.replace(/\(自動検知\)/, "");
	}
	get("#melody-key").options[index].text += "(自動検知)";
}
function forceLoadInvalidFile(fileType = ""){
	return confirm(`それはMIDIファイルではありません(ファイルタイプ: ${fileType})。\nしかし、ファイルを強制的に読み込むこともできます。\n読み込みますか？`);
}
let notes = [];
let parseData;
let picoAudio;
function parseSmf(smfData){
	picoAudio = new PicoAudio();
	picoAudio.init();
	parseData = picoAudio.parseSMF(smfData);
	picoAudio.setData(parseData); // 本当は要らないけど picoAudio をF12で見たいのでつけた

	header.resolution = parseData.header.resolution;
}
function parseNote(){
	for (let i = 0; i < parseData.channels.length; i++) {
		notes[i] = [];
	}

	const enabledChannels = [MIDI_CH_CHORD, MIDI_CH_MELODY];
	for (let i = 0; i < enabledChannels.length; i++) {
		const channelNum = enabledChannels[i];
		const channelNotes = parseData.channels[channelNum].notes;
		for (let k = 0; k < channelNotes.length; k++) {
			const note = channelNotes[k];
			notes[channelNum].push({
				time: note.start,
				gate: note.stop - note.start,
				note: note.pitch,
				count: 0,
			});
		}
	}
	
	// 重かったので必要なところだけソートすることにした
	// notesは発音終了の早い順でノートが格納されているので、それを発音開始の早い順にソートする
	notes[MIDI_CH_CHORD].sort((a, b) => (a.time < b.time ? -1 : 1));
	notes[MIDI_CH_MELODY].sort((a, b) => (a.time < b.time ? -1 : 1));
}
function afterLoadMidi(midiUint8Array, fileName) {
	//データをparserに渡す

	/*let text = "";
	for(let i=0; i<midiUint8Array.length; i++){
		text += midiUint8Array[i] + ",";
		if(i%50 === 49){
			text += "\n"
		}
	}
	console.log(text)*/
	parseSmf(midiUint8Array);
	parseNote();
	stopAnimation();
	drawMessage(fileName);
	initializeMainGroup();
	drawBarLine(pianoRollRatioX, 0);
	drawNote();
	midiFileName = fileName;
}

function loadFile(file) {
	const fileIsNotMid = file.type !== "audio/mid";
	if (fileIsNotMid && !forceLoadInvalidFile(file.type)) {
		return;
	}
	let reader = new FileReader();

	reader.onload = function() {
		//読み込んだ結果を型付配列に
		let ar = new Uint8Array(reader.result);
		afterLoadMidi(ar, file.name);
	};
	//ファイルを読み込み
	reader.readAsArrayBuffer(file);
}
// https://qiita.com/PianoScoreJP/items/480c694f00a4f3afb28e
document.getElementById("load-file").addEventListener(
	"change",
	function(e) {
		let file = e.target.files[0];
		loadFile(file);
	},
	false,
);

function clearSvgNote() {
	//for(let i=0; i<noteRects.length; i++){
	//	noteRects[i].remove();
	//}
	svgContainer.noteRects = [];
	svgGroup.note.clear();
}
function drawAnalyzingFormatText(x) {
	switch (x) {
		case ANALYZING_FORMAT_SHELL_ONLY:
			return "K/S format: " + "S：音符色・白文字";
		case ANALYZING_FORMAT_KERNEL_ONLY:
			return "K/S format: " + "K：音符色・白文字";
		default:
		case ANALYZING_FORMAT_KERNEL_AND_SHELL:
			return "K/S format: " + "K：音符色、S：文字色";
	}
}
function drawMessage(fileName) {
	svgGroup.message.clear();
	const paddingX = 80;
	const paddingY = 10;
	{
		const text = canvas.text(fileName);
		text.x(paddingX);
		text.y(paddingY);
		text.font({
			fill: colorDefinition.message,
			family: "Helvetica",
			size: 15,
		});
		svgGroup.message.add(text);
	}
	{
		svgContainer.message.capo = canvas.text("capo: " + getCapoString(capo));
		svgContainer.message.capo.x(paddingX);
		svgContainer.message.capo.y(paddingY + 25);
		svgContainer.message.capo.font({
			fill: colorDefinition.message,
			family: "Helvetica",
			size: 15,
		});
		svgGroup.message.add(svgContainer.message.capo);
	}
	{
		svgContainer.message.analyzingFormat = canvas.text(drawAnalyzingFormatText(analyzingFormat));
		svgContainer.message.analyzingFormat.x(paddingX);
		svgContainer.message.analyzingFormat.y(paddingY + 25 * 2);
		svgContainer.message.analyzingFormat.font({
			fill: colorDefinition.message,
			family: "Helvetica",
			size: 15,
		});
		svgGroup.message.add(svgContainer.message.analyzingFormat);
	}
}
function drawWarningMessage() {
	const paddingX = 80;
	const paddingY = 10;
	svgContainer.message.warning = canvas.text("音符の数が" + maxNoteNotes + "を超えているため描画を打ち切りました。");
	svgContainer.message.warning.x(paddingX);
	svgContainer.message.warning.y(paddingY + 25 * 3);
	svgContainer.message.warning.font({
		fill: colorDefinition.message,
		family: "Helvetica",
		size: 15,
	});
	svgGroup.message.add(svgContainer.message.warning);
}
function drawBarLine() {
	svgGroup.barLine.clear();

	// コンダクタートラックから拍子情報を読み取る
	let beats = [];

	for (let i = 0; i < parseData.beatTrack.length; i++) {
		const beatTrack = parseData.beatTrack[i];
		beats.push({
			time: beatTrack.timing,
			numerator: beatTrack.value[0],
			denominator: beatTrack.value[1],
		});
	}
	const trackEndTime = parseData.lastNoteOffTiming;

	if (beats.length === 0) {
		beats.push({
			time: 0,
			numerator: 4,
			denominator: 4,
		});
	}
	beats.push({
		time: trackEndTime + 1,
		numerator: beats[beats.length - 1].numerator,
		denominator: beats[beats.length - 1].denominator,
	});

	// 描画
	let barLineNum = 0;
	let barNum = 0;
	for (let i = 0; i < beats.length - 1; i++) {
		const beat = beats[i];
		const dx = (beat.denominator / 4) * quarterNoteWidth;
		const xStart = gateTimeToPixel(beat.time);
		const xEnd = gateTimeToPixel(beats[i + 1].time); // x: timeToPixel(note.time)
		let cnt = 0;
		for (let x = xStart; x < xEnd; x += dx) {
			const isBarOfHead = cnt % beat.numerator === 0;
			const lineColor = isBarOfHead ? colorDefinition.barLine.head : colorDefinition.barLine.body;
			const lineWidth = 2;
			let line = canvas.line(x, 0, x, bottomY);
			if (isBarOfHead) {
				let text = canvas
					.text("" + (barNum + 1))
					.x(x + 2)
					.y(5)
					.font({
						fill: colorDefinition.barLine.numberText,
						family: "Helvetica",
						size: 12,
					});
				svgContainer.barLineTexts[barNum] = text;
				svgGroup.barLine.add(text);
				barNum += 1;
			}
			line.stroke({
				width: lineWidth,
				color: lineColor,
			});
			svgGroup.barLine.add(line);
			svgContainer.barLines[barLineNum] = line;
			cnt += 1;
			barLineNum += 1;
		}
	}
}
function gateTimeToPixel(gateTime) {
	return (quarterNoteWidth * gateTime) / header.resolution;
}
function pixelToGateTime(x) {
	return (x * header.resolution) / quarterNoteWidth;
}
function drawNote() {
	clearSvgNote();

	let prevNoteMin = 127;
	{
		const fillColor = colorDefinition.note.normal.chord;
		const strokeColor = colorDefinition.strokeDefault;
		let cnt = 0;

		noteRectEndX = 0;

		// ここでもソート
		let chordAndMelodyNotes = notes[MIDI_CH_CHORD].concat(notes[MIDI_CH_MELODY]);
		chordAndMelodyNotes.sort((a, b) => (a.time < b.time ? -1 : 1));

		for (let k = 0; k < chordAndMelodyNotes.length; k++) {
			if (maxNoteNotes <= cnt) {
				drawWarningMessage();
				break;
			}
			const note = chordAndMelodyNotes[k];
			note.count = cnt;
			const rectWidth = gateTimeToPixel(note.gate);
			const x = gateTimeToPixel(note.time);
			svgContainer.noteRects[cnt] = canvas.rect(rectWidth, noteHeight);
			svgContainer.noteRects[cnt].fill(fillColor);
			svgContainer.noteRects[cnt].stroke(strokeColor);
			svgContainer.noteRects[cnt].move(x, noteHeight * (127 - note.note));
			if (note.note < prevNoteMin) {
				prevNoteMin = note.note;
			}
			if (noteRectEndX < x + rectWidth) {
				noteRectEndX = x + rectWidth;
			}
			noteRectPropaties[cnt] = {
				colorMaps: [
					{
						normal: fillColor,
						sounding: colorDefinition.note.sounding.chord,
					},
				],
			};
			svgGroup.note.add(svgContainer.noteRects[cnt]);
			cnt++;
		}
	}
	noteMin = prevNoteMin;
	analyzeKey();
	analyze();
}
function get(str) {
	var identifier = str.substr(0, 1);
	if (identifier === "#") {
		return document.getElementById(str.substr(1));
	} else if (identifier === ".") {
		return document.getElementsByClassName(str.substr(1));
	} else if (identifier === "@") {
		return document.getElementsByName(str.substr(1));
	} else {
		return document.getElementsByTagName(str);
	}
}

function dropEvent(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	let files = evt.dataTransfer.files;
	loadFile(files[0]);
}
function dragOverEvent(evt) {
	evt.stopPropagation();
	evt.preventDefault();
	evt.dataTransfer.dropEffect = "copy";
}

window.addEventListener("load", () => {
	drawBackground();
	drawKeyboard();
	drawPlayCursor();
	windowResize();
	if (!localStorage.getItem(localStorageKey.soundVolume)) {
		localStorage.setItem(localStorageKey.soundVolume, "15");
	}
	setAuditionSoundVolume(localStorage.getItem(localStorageKey.soundVolume));
	document.body.addEventListener("dragover", dragOverEvent, false);
	document.body.addEventListener("drop", dropEvent, false);
});
