// 
// save-fig.coffee(https://gist.github.com/jkawamoto/657be368dbec2e8750a50e071789bc29 )よりJavaScriptに改変
//
// save-fig.coffee
//
// Copyright (c) 2016 Junpei Kawamoto
//
// save-fig.coffee is released under the MIT License.
//
// http://opensource.org/licenses/mit-license.php
// 

"use strict";

const svg2png = (svgElement, fileName = "output") => {
	const encode = (data) => btoa(unescape(encodeURIComponent(data)));
	let svg = svgElement;
	const width = canvas.width();
	const height = canvas.height();
	let canvasElement = document.createElement('canvas');//document.getElementById("canvas-for-converting-to-png");
	let ctx = canvasElement.getContext("2d");
	canvasElement.width = width;
	canvasElement.height = height;
	const svgData = new XMLSerializer().serializeToString(svg);
	const imgsrc = "data:image/svg+xml;charset=utf-8;base64," + (encode(svgData));
	let image = new Image();
	image.onload = () => {
		ctx.fillStyle = "#FFF";
		ctx.fillRect(0, 0, width, height);
		ctx.drawImage(image, 0, 0);
		let a = document.createElement('a');
		a.download = decodeURI(fileName + ".png");
		a.href = canvasElement.toDataURL("image/png");
		a.type = 'application/octet-stream';
		a.click();
	};
	image.src = imgsrc;
};
