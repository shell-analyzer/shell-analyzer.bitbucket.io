# Shell-Analyzer v1.5.5
![タイトルスクリーンショット](https://bitbucket.org/shell-analyzer/shell-analyzer.bitbucket.io/raw/dd20515f1a9b45178c9f23cd8247b7c8be508320/img/README.md/title-screenshot.png)


## URL
[https://shell-analyzer.bitbucket.io/](https://shell-analyzer.bitbucket.io/)

推奨ブラウザー：Google Chrome


## 簡単な説明
メロディがコードのルートに対してどのくらいの度数離れているかを表示するピアノロールです。

"Shell"(シェル)に関する詳しい説明は [SoundQuest](https://soundquest.jp/quest/) さんへ


## ピアノロールのスクリーンショット
ピアノロールのスクリーンショット・録画はSNSでの共有や動画サイトへのアップロードに使えます。ただし、曲の著作権等には注意してください。
アップロードの際はリンクやクレジット表示をしなくても構いませんが、文字数に余裕がある場合はしてくださると嬉しいです。


## 使い方
### 読み込む MIDI ファイルについて
- フォーマットは 0 と 1 両方読み込めます
- MIDIファイルがインターネットにアップロードされるわけではないので、著作権関連の心配はいりません

### 読み込む MIDI ファイルの構成について
- Ch1 がベース+コード
- Ch2 がメロディ

という構成でお願いします
  
### 推奨 MIDI シーケンサー
- Domino

### キーボード・マウスで操作するもの・ショートカット
|動作|ショートカット|
|---|---|
|MIDI読み込み|画面にドラッグ&ドロップ|
|拡大縮小|Ctrl + ホイール|
|左右移動|ホイールクリック|
|上下移動|Shift + ホイールクリック|
|カーソル移動|左クリック|

### PNG エクスポート
![PNGエクスポートのスクリーンショット](https://bitbucket.org/shell-analyzer/shell-analyzer.bitbucket.io/raw/ae803474aedc2b2f824723c64eb0ae2fb863b89f/img/README.md/png-export.png)

ピアノロールのスクリーンショットを PNG ファイルとして保存できます。

## 読み方
要は緑ほど安定、赤に行くほど不安定になります。

### シェルモード
![シェルモード説明用画像](https://bitbucket.org/shell-analyzer/shell-analyzer.bitbucket.io/raw/ae803474aedc2b2f824723c64eb0ae2fb863b89f/img/README.md/legend-shell-mode-screenshot.png)

|色|度数|備考|
|---|---|---|
|緑|Root、P5th、♭5th、♯5th||
|黄緑|m3rd、M3rd||
|橙|m7th、M7th||
|赤|上記以外|要はノンコードトーン|

- 以下のテンションとの区別はコードによってなされます
  - ♯9 (♯2) と m3 
  - ♯11 (♯4) と ♭5
  - ♭13 (♭6) と ♯5
- P1、M2、M3、P4、P5、M6、m7、M7は特に何も付けず数字だけ表示しています
  - m7 と M7 が同時に出てくることはないと考え、これらをひとくくりに 7th と描画しています
- スラッシュコード(C/E)では、分子と分母両方に対するシェルを表示しています
- 変位シェルは数字を青くしています (色は今後変更の予定あり)

### カーネルモード
![カーネルモード説明用画像](https://bitbucket.org/shell-analyzer/shell-analyzer.bitbucket.io/raw/ae803474aedc2b2f824723c64eb0ae2fb863b89f/img/README.md/legend-kernel-mode-screenshot.png)

|色|階名|説明|
|---|---|---|
|緑|ド、ミ、ソ||
|黄緑|レ、ラ||
|黄|シ||
|橙|ファ||
|赤|黒鍵|その他|

![カーネルモードの文字のスクリーンショット](https://bitbucket.org/shell-analyzer/shell-analyzer.bitbucket.io/raw/dd20515f1a9b45178c9f23cd8247b7c8be508320/img/README.md/kernel-mode-charactor.png)

カーネルモードは、上のスクリーンショットのように色々な文字が利用可能です。


## キーについて
![キーのスクリーンショット](https://bitbucket.org/shell-analyzer/shell-analyzer.bitbucket.io/raw/dd20515f1a9b45178c9f23cd8247b7c8be508320/img/README.md/key-auto-detect.png)

キーはメロディから自動で検出されます。また、キーは手動で変更できます。

## リンク
- qn-roller (シマフリさん) https://github.com/shimafuri/qn-roller

## デザイン協力
- たろいもさん https://twitter.com/musicstd

## 参考
- シマフリさんのツイート https://twitter.com/shimafuri/status/1035121201687932928
- SoundQuest/Quest メロディ編 Ⅰ、Ⅱ、Ⅳ章 https://soundquest.jp/quest/
- Web Audio API 解説 - 01.前説 | g200kg Music & Software https://www.g200kg.com/jp/docs/webaudio/

## コードについて
このコードは SVG.js と PicoAudio.js と note2chord.js を使用し、save-fig.coffee を改変して使用しています。

[SVG.js](http://svgdotjs.github.io/) © 2012-2018 [Wout Fierens](https://github.com/wout/) - SVG.js is released under the terms of the MIT license.

[PicoAudio.js](https://github.com/cagpie/PicoAudio.js) Copyright (c) 2017 coba

[note2chord.js](https://bitbucket.org/note2chordjs/note2chordjs.bitbucket.io/) Copyright (c) 2020 36kHz

[save-fig.coffee](https://gist.github.com/jkawamoto/657be368dbec2e8750a50e071789bc29) Copyright (c) 2016 Junpei Kawamoto - save-fig.coffee is released under the MIT License.

